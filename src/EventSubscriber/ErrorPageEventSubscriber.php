<?php

namespace Drupal\random_404_page\EventSubscriber;

use Drupal\Core\EventSubscriber\CustomPageExceptionHtmlSubscriber;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Event\ExceptionEvent;

/**
 * Subscribes to 404 and 403 exception events.
 */
class ErrorPageEventSubscriber extends CustomPageExceptionHtmlSubscriber {

  /**
   * Make a sub-request to a randomly selected page.
   *
   * @param \Symfony\Component\HttpKernel\Event\ExceptionEvent $event
   *   The event to process.
   * @param string $configName
   *   Config key containing the list of pages.
   * @param int $statusCode
   *   The status code for the error being handled.
   */
  private function makeSubrequestRandomPage(ExceptionEvent $event, string $configName, int $statusCode) {
    $pages = $this->configFactory->get('random_404_page.settings')
      ->get($configName);

    if (!empty($pages)) {
      $this->makeSubrequestToCustomPath($event, $pages[array_rand($pages)], $statusCode);
    }
  }

  /**
   * {@inheritdoc}
   */
  public function on404(ExceptionEvent $event) {
    $this->makeSubrequestRandomPage($event, '404_pages', Response::HTTP_NOT_FOUND);
  }

  /**
   * {@inheritdoc}
   */
  public function on403(ExceptionEvent $event) {
    $this->makeSubrequestRandomPage($event, '403_pages', Response::HTTP_FORBIDDEN);
  }

  /**
   * {@inheritdoc}
   */
  protected function getHandledFormats(): array {
    return ['html'];
  }

  /**
   * {@inheritdoc}
   */
  protected static function getPriority(): int {
    // Before CustomPageExceptionHtmlSubscriber.
    return parent::getPriority() + 1;
  }

}
